#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main (int argc, char * argv[]) {
    int errorReading = 0;
    char DATA[256];
    while(1)
        {
            int pid = fork();
            if (pid == 0) {
                char *argv1[10];
                argv1[0] = argv[2];  argv1[1] = DATA;
                execvp("./data.o", argv1);
            }        
            int pid2 = fork();
            if (pid2 == 0) {
                if(DATA[0] == 'W' || (DATA[0] == 'R' && errorReading == 1))
                {
                char *argv2[10];
                argv2[0] = argv[3];  argv2[1] = DATA;
                errorReading = 0;
                execvp("./data.o", argv2);
                }
                else return 0;
            }
        if(pid >0 && pid2 > 0)
        {
        int status,status2;
        waitpid(pid, &status, 0);
        waitpid(pid2, &status2, 0);
        if(DATA[0] == 'Q')
            return 0;
        if(status != 0)
        {
            errorReading = 1;
        }
            fgets(DATA,sizeof(DATA),stdin);
        }
    }
    return 0;
}
