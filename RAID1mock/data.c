#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    char TYPE = argv[1][0];
    char DATA[256];
    strncpy(DATA, argv[1] + 2, 253);
    char *f = argv[0];

    if(TYPE == 'R')
    {
        int lineNum = atoi(DATA);
        FILE *fp = fopen(f,"r");
        if(fp == NULL)
        {
            return -1;
        }
        int count = 0; 
        char line[256]; 
        while (fgets(line, sizeof line, fp) != NULL) 
        {
            if (count == lineNum)
            {
                char returnLine[256];
                strcpy(returnLine,line);
                printf(returnLine);
                //printf(line);
            }
            else
            {
                count++;
            }
        }
        fclose(fp);
    }
    else if(TYPE == 'W')
    {
        FILE *fp = fopen(f,"r+");
        if(fp == NULL)
        {
            return -1;
        }
        else
        {
            fclose(fp);
            fopen(f,"a");
            fprintf(fp,DATA,'\0');
            fclose(fp);
        }
    }
    else
    {
        return 0;
    }
    return 0;
}
